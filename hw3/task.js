// Завдання 1//

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// const clientArr = [...clients1,...clients2]

// const makeUniq = (clientArr) =>{
//     const uniqSet = new Set(clientArr);
//     return [...uniqSet]
// }
// console.log(makeUniq(clientArr))




//Завдання 2 //

//Перед вами массив characters, що складається з об'єктів. Кожен об'єкт описує одного персонажа.
// Створіть на його основі масив charactersShortInfo, 
// що складається з об'єктів, у яких є тільки 3 поля - ім'я, прізвище та вік.


// const characters = [
//     {
//       name: "Елена",
//       lastName: "Гилберт",
//       age: 17, 
//       gender: "woman",
//       status: "human"
//     }

//   ];

// const [name,lastName,age] = characters






//Завдання 3//

// const user1 = {
//     name1: "John",
//     years: 30
//   };

//   const {name1 , years, isAdmin = false} = user1


// const namee = user1.name1
// const yearss = user1.years
// const isAdmin1 = user1.isAdmin


// console.log(namee);
// console.log(yearss)
// console.log(isAdmin1)

//Завдання 4//



// Детективне агентство кілька років збирає інформацію про можливу особистість Сатоши Накамото. Вся інформація, 

// зібрана у конкретному році, зберігається в окремому об'єкті. Усього таких об'єктів три - satoshi2018, satoshi2019, satoshi2020.

// Щоб скласти повну картину та профіль, вам необхідно об'єднати дані з цих трьох об'єктів в один об'єкт - fullProfile.

// Зверніть увагу, що деякі поля в об'єктах можуть повторюватися. У такому випадку в результуючому об'єкті має зберегтися значення, яке було отримано пізніше (наприклад, значення з 2020 пріоритетніше порівняно з 2019).

// Напишіть код, який складе повне досьє про можливу особу Сатоші Накамото. Змінювати об'єкти satoshi2018, satoshi2019, satoshi2020 не можна.

// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//       lat: 38.869422, 
//       lng: 139.876632
//     }
//   }
  
//   const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
//   }
  
//   const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto', 
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
//   }

//   let SatoshiArr = {...satoshi2018,...satoshi2019,...satoshi2020}
//   console.log(SatoshiArr)





  //Завдання 5//

//   Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).

//   const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];
  
//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }


//   let allBooks = [...books,bookToAdd]
//   console.log(allBooks)





//Завдання 6

// Даний об'єкт employee. Додайте до нього властивості age і salary, 
// не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). В
// иведіть новий об'єкт у консоль.

// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
//   }


  
// const addEmployee = {
//     age : 35,
//     salary :'2500$'
// }
// let aboutEmployee  =  {...employee,...addEmployee}
// console.log(aboutEmployee)






////Завдання 7///

// Доповніть код так, щоб він коректно працював


// const array = ['value', () => 'showValue'];
// const[value,showValue] = array


// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'
  



